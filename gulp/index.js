'use strict';

// semantic-ui tasks
var
gulp  = require('gulp'),
watch = require('../semantic/tasks/watch.js'),
build = require('../semantic/tasks/build.js')
;
// import task with a custom task name
gulp.task('watch ui', watch);
gulp.task('build ui', build);

// app
var fs = require('fs');
var onlyScripts = require('./util/script-filter');
var tasks = fs.readdirSync('./gulp/tasks/').filter(onlyScripts);

tasks.forEach(function(task) {
  require('./tasks/' + task);
});
