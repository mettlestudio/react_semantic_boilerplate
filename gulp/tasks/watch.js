'use strict';

import gulp   from 'gulp';
import config from '../config';
import browserSync  from 'browser-sync';

gulp.task('watch', ['browserSync'], function() {

    // Scripts are automatically watched by Watchify inside Browserify task
    gulp.start('watch ui');
    gulp.watch(config.style.dest + '*').on('change', browserSync.reload);
    gulp.watch(config.images.src,                 ['imagemin']);
    gulp.watch(config.sourceDir + 'index.html',   ['copyIndex']);
    
});
