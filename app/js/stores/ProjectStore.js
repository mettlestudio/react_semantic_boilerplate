'use strict';

import Reflux             from 'reflux';

import ProjectActions     from '../actions/ProjectActions';

const ProjectStore = Reflux.createStore({

  init() {
    this.name = null;

    // this.listenTo(CurrentUserActions.logout, this.logoutUser);
  },

  throwError(err) {
    this.trigger(err);
  }

});

export default ProjectStore;
